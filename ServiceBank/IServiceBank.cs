﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServiceBank.Model;
namespace ServiceBank
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServiceBank
    {
        string RealizarTransaccion(string cuentaOrigen, string cuentaDestino, int saldo);
        decimal ConsultarSaldo(string cuenta);
        List<Transaccion> ObtenerMovimientos();
    }
}
