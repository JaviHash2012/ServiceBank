﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceBank.Model
{
    [DataContract]
    public class Cliente
    {
        [DataMember]
        public int Id_cliente
        {
            get;
            set;
        }
        [DataMember]
        public string Nombre_cliente
        {
            get;
            set;
        }
        [DataMember]
        public string Apellido_paterno
        {
            get;
            set;
        }
        [DataMember]
        public string Apellid_materno
        {
            get;
            set;
        }
        [DataMember]
        public string Telefono
        {
            get;
            set;
        }
        [DataMember]
        public string Calle
        {
            get;
            set;
        }
        [DataMember]
        public string No_int
        {
            get;
            set;
        }
        [DataMember]
        public string No_ext
        {
            get;
            set;
        }
        [DataMember]
        public string Colonia
        {
            get;
            set;
        }
        [DataMember]
        public string Ciudad
        {
            get;
            set;
        }
        [DataMember]
        public string Estado
        {
            get;
            set;
        }
    }
}