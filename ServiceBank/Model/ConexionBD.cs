﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ServiceBank.Model
{
    public class ConexionBD
    {
        /// <summary>
        ///Variables globales
        /// </summary>
        private string database;
        private string userId;
        private string password;
        private string server;

        private MySqlConnectionStringBuilder cadenaConexion;

        public ConexionBD()
        {
            database = "numeros";
            userId = "root";
            password = "ISWHash2012Creative1220";
            server = "localhost";
        }

        
        public MySqlConnection Conectar()
        {
            cadenaConexion = new MySqlConnectionStringBuilder()
            {
                Database = database,
                UserID = userId,
                Password = password,
                Server = server
            };
            return new MySqlConnection(cadenaConexion.ToString());
        }
    }
}