﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceBank.Model
{
    [DataContract]
    public class Cuenta
    {
        [DataMember]
        public string Clave
        {
            get;
            set;
        }
        [DataMember]
        public string Vigencia
        {
            get;
            set;
        }
        [DataMember]
        public decimal Saldo
        {
            get;
            set;
        }
        [DataMember]
        public string Id_Cliente
        {
            get;
            set;
        }
    }
}