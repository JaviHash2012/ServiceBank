﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceBank.Model
{
    [DataContract]
    public class Transaccion
    {
        [DataMember]
        public int Id_Transaccion
        {
            get;
            set;
        }
        [DataMember]
        public string Tipo_transaccion
        {
            get;
            set;
        }
        [DataMember]
        public string Fecha
        {
            get;
            set;
        }
        [DataMember]
        public string Cuenta
        {
            get;
            set;
        }
    }
}